﻿namespace MeridixStudio.UserImportUtil
{
    public class CsvUserRecord
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public bool Administrator { get; set; }
        public bool ViewAllEnabled { get; set; }
    }
}