﻿using CsvHelper.Configuration;

namespace MeridixStudio.UserImportUtil
{
    public sealed class CsvUserRecordMap : ClassMap<CsvUserRecord>
    {
        public CsvUserRecordMap()
        {
            Map(m => m.Email).Name("Email");
            Map(m => m.Name).Name("Name");
            Map(m => m.Administrator).Name("Administrator");
            Map(m => m.ViewAllEnabled).Name("ViewAllEnabled");
        }
    }
}