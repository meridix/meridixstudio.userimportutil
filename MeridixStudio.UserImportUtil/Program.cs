﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using CsvHelper;
using CsvHelper.Configuration;
using Meridix.Studio.Api;
using Meridix.Studio.Api.DataTransferObjects;

namespace MeridixStudio.UserImportUtil
{

    public class Options
    {
        [Option('u', "meridix-url", Required = true, HelpText = "The Meridix base url e.g. https://reports.vendor.com")]
        public string MeridixUrl { get; set; }

        [Option('t', "meridix-token", Required = true, HelpText = "The Meridix API ticket token")]
        public string MeridixToken { get; set; }

        [Option('s', "meridix-secret", Required = true, HelpText = "The Meridix API ticket secret")]
        public string MeridixSecret { get; set; }

        [Option('f', "source-file", Required = true, HelpText = "The relative or rooted path to the source CSV file containg the users")]
        public string SourceFilePath { get; set; }
        [Option('c', "target-customer-id", Required = false, HelpText = "The customer id to import users into. Only required for system level tickets.")]
        public int TargetCustomerId { get; set; }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) => Console.WriteLine(((Exception)eventArgs.ExceptionObject).Message);

            CommandLine.Parser.Default
                .ParseArguments<Options>(args)
                .WithParsed<Options>(RunOptionsAndReturnExitCode);
        }

        private static void RunOptionsAndReturnExitCode(Options options)
        {
            var sourceFilePath = Path.IsPathRooted(options.SourceFilePath) ? options.SourceFilePath : Path.Combine(AppDomain.CurrentDomain.BaseDirectory, options.SourceFilePath);
            var records = new List<CsvUserRecord>();
            var csv = new CsvReader(File.OpenText(sourceFilePath), new Configuration()
            {
                Delimiter = ";"
            });
            csv.Configuration.RegisterClassMap<CsvUserRecordMap>();
            using (csv)
            {
                records.AddRange(csv.GetRecords<CsvUserRecord>());
            }

            var client = MeridixStudioClient.Create(options.MeridixUrl, options.MeridixToken, options.MeridixSecret);
            client.UseHttpMethodOverride = true;
            client.OnOutput += (sender, args) =>
            {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(args.Message);
                Console.ResetColor();
            };
            client.ValidateApiAccess();

            if (options.TargetCustomerId == 0)
            {
                options.TargetCustomerId = client.GetTicketCustomerId().GetValueOrDefault();
            }

            var userApi = client.UserApi(options.TargetCustomerId);
            var existingUsers = userApi.List();

            foreach (var userRecord in records)
            {
                var userDto = new UserDto
                {
                    CustomerId = options.TargetCustomerId,
                    Email = userRecord.Email,
                    Username = userRecord.Email,
                    Fullname = userRecord.Name,
                    IsAdministrator = userRecord.Administrator,
                    ViewAllEnabled = userRecord.ViewAllEnabled
                };
                
                // Temporary fix. Newer releases of Meridix automatically handle empty password fiels for added user by creating them on the server.
                var password = Guid.NewGuid().ToString();
                var salt = BCrypt.Net.BCrypt.GenerateSalt(15);
                var hashedPassword = BCrypt.Net.BCrypt.HashPassword(password, salt);
                userDto.Password = hashedPassword;
                userDto.PasswordSalt = salt;

                if (existingUsers.All(eu => eu.Email != userDto.Email))
                {
                    userApi.Add(userDto);
                    userApi.SendUserInitiationRequestMail(userDto);
                    Console.WriteLine($"Added user {userDto.Email} successfully");
                }
                else
                {
                    Console.WriteLine($"User {userDto.Email} already exist and was therefor skipped");
                }
            }
        }
    }
}
